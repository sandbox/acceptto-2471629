<?php

  global $acceptto_titles;
  global $acceptto_descriptions;
  global $acceptto_error;

/**
 * Titles
 */
  // Config title strings
  $acceptto_titles['config_form'] = t('Acceptto multi factor configuration');
  $acceptto_titles['integration_key'] = t('Uid');
  $acceptto_titles['secret_key'] = t('Secret');
  $acceptto_titles['api_hostname'] = t('API hostname');
  $acceptto_titles['auth_message'] = t('Authentication Message');

  // Permission title string
  $acceptto_titles['permission'] = t('Log in with Acceptto multi factor authentication');


/**
 * Descriptions
 */

  // Config description strings
  $acceptto_descriptions['config_form'] = t('Setup the Acceptto module with your integration settings.');
  $acceptto_descriptions['integration_key'] = t('<a href="https://mfa.acceptto.com/organisations/application">uid from the Acceptto relying party control panel</a>');
  $acceptto_descriptions['secret_key'] = t('<a href="https://mfa.acceptto.com/organisations/application">secret from the Acceptto relying party control panel</a>');
  $acceptto_descriptions['api_hostname'] = t('<a href="https://mfa.acceptto.com/organisations/application">API hostname from the Acceptto relying party control panel</a>');
  $acceptto_descriptions['acceptto_auth_message'] = t('Message which will be shown to user on mobile while authenticating, for example: Drupal wishes to auhtorize.');

  // Permission description string
  $acceptto_descriptions['permission'] = t('Require the selected roles to authenticate with two-factor authentication.');
  $acceptto_descriptions['tfa_success'] = t('Multi factor authentication was successfull.');


/**
 *  Error Messages
 */

  // Login errors
  $acceptto_error['logged_in'] = t("You are already logged in.");
  $acceptto_error['login_invalid'] = t("Invalid username or password.");

  // Config errors
  $acceptto_error['invalid_ikey'] = t("The uid you have entered is the incorrect length.  Please check the value before trying again.");
  $acceptto_error['invalid_skey'] = t("The secret you have entered is the incorrect length.  Please check the value before trying again.");
  $acceptto_error['short_api'] = t("The API hostname you have entered is too short.  Please check the value before trying again.");
  $acceptto_error['bad_configuration'] = t("The module hasn't been configured properly.  Check your settings before trying again.");
  $acceptto_error['invalid_configuration'] = t("The configuration values are incorrect! Please refer to your <a href='https://mfa.acceptto.com/organisations/application'>Dashboard</a> for obtaining correct configuration values!");

  // Two-factor errors
  $acceptto_error['tfa_not_recieved'] = t("The required information wasn't received.  Please try again.");
  $acceptto_error['tfa_invalid'] = t("Secondary authentication credentials were invalid, or the user cannot be loaded.");
  $acceptto_error['tfa_session_expired'] = t("Secondary authentication session is expired, Please try again!");
  $acceptto_error['tfa_authentication_failed'] = t("Two factor authentication failed!");

  // PHP version error
  $acceptto_error['php_invalid'] = t("You are running an unsupported") . " PHP " . t("installation.") . " PHP >= 5.1.2, PECL >= 1.1" . t("required.");
