** Acceptto Two-Factor Authentication Module **

Drupal Version:  7.x
Module Version: 1.8
Contributors:  Acceptto
Contact:  info@acceptto.com


Description
-----------

This module adds Acceptto two-factor authentication to your Drupal site.


Installation
------------

For detailed installation and configuration instructions, please visit:

    http://www.acceptto.com/docs/drupal


Contact
-------

Have a question? Drop us a line at info@acceptto.com.


Changelog
---------

1.0 - Initial Release


File List
---------

  acceptto /
    acceptto.info
    acceptto.install
    acceptto.php
    acceptto.module
    acceptto_strings.php
    README.txt
