<?php


/**
 *  Generates Help Text.
 *
 *  This is just to remove all HTML from the module itself.
 *
 *  @return Help HTML
 */
  function _acceptto_generate_help() {
    $help = '<h1>' . t("Acceptto Multi Factor Authentication") . '</h1>';

    $help .= "<p>" . t("For more complete documentation, view Acceptto's") . ' <a href="http://www.acceptto.com/docs/drupal">' . t('documentation on the module') . "</a>.</p>";

    $help .= "<p>" . t("If you need additional help, contact") . ' <a href="mailto:support@acceptto.com">support@acceptto.com</a>.</p>';

    $help .= '<h2>' . t("About") . '</h2>';

    $help .= '<p>' . t("This module adds a second step to Drupal's existing login function by implementing") . " Acceptto's " . t("multi factor authentication API.") . '</p>';

    $help .= '<p>' . t("Acceptto's multi factor authentication requires a user to use their phone to log in, in addition to their username and password. After entering a valid username and password, the user will be asked to enroll with acceptto. After they register their phones with acceptto, they will be asked to authenticate with their phones before being signed in.") .  '</p>';

    $help .= '<h2>' . t("Setting up") . '</h2>';

    $help .= '<p>' . t("To set up this module, you need to sign up with an account at") . ' <a href="http://www.acceptto.com" target="_blank">http://www.acceptto.com</a>.</p>';

    $help .= '<p>' . t("After signing up with Acceptto, you will need to create an Acceptto. ") . t("Acceptto offers a") . ' <a href="http://www.acceptto.com/docs">' . t("getting started guide") . "</a> " . t("that explains how to create an application.") .'</p>';

    $help .= '<p>' . t("Upon creating an integration, you will be provided with an integration key, secret key, and API hostname. You'll need this information to configure the module.  Copy and paste the values from the Acceptto Relying Party dashboard into the module configuration page.") . '</p>';

    $help .= '<p>' . t("If the acceptto plugin is left unconfigured, users will continue to login as usual without multi factor authentication.") . '</p>';

    $help .= '<h2>' . t("Customization") . '</h2>';

    $help .= '<p>' . t("You can customize the look of the login page by editing the files") . " acceptto_header.php, acceptto_footer.php " . t("and") . " custom.css " . t("in the module's") . " resources " . t("folder. The login form is rendered in between the header and footer files.");

    return $help;
  }

  /**
   * Helper method for calling rest api of server
   * @param $url REST address
   * @return response of server|null
   */
  function get_curl_url($url) {
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, TRUE);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
    curl_setopt( $ch, CURLOPT_PROXYTYPE, CURLPROXY_HTTP );

    $response = json_decode(curl_exec($ch));
    curl_close ($ch);
    if (!$response){
      return NULL;
    }
    return $response;
  }

  /**
   * Gets host of url
   * @return string
   */
  function get_host_url() {
    $protocol = (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' || $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://";
    return $protocol.$_SERVER['HTTP_HOST'];
  }
